import numpy as np
import unittest

'''
----------------------------------------
measure of goodness function definitions
----------------------------------------
'''


def tracking_error(index_return, replicating_return):
    """Calculate the standard deviation of difference between daily index return and replicating portfolio return
    Parameters
    ----------
    index_return (list): daily return of benchmark index
    replicating_return (list): daily return of replicating portfolio
    Returns
    -------
    float containing standard deviation of differences between daily index return and replicating portfolio return
    """
    index_return = np.asarray(index_return)
    replicating_return = np.asarray(replicating_return)
    # return sample standard deviation
    return np.std(index_return - replicating_return, ddof=1)


def tracking_difference(index_return, replicating_return):
    """Calculate the mean absolute value of differences between daily index return and replicating portfolio return
    Parameters
    ----------
    index_return (list): daily return of benchmark index
    replicating_return (list): daily return of replicating portfolio
    Returns
    -------
    float containing mean absolute value of differences between daily index return and replicating portfolio return
    """
    index_return = np.asarray(index_return)
    replicating_return = np.asarray(replicating_return)
    # return sample standard deviation
    return np.mean(np.abs(index_return - replicating_return))


'''
-----
ToDo: Return decomposition by examining coefficients on different factors
-----
'''


def jaccard_similarity(index_members, replication_members):
    """Calculate similarity between two sets of members in index constituents and replication portfolio members,
    by computing the ratio of intersection cardinality to union cardinality
    Parameters
    ----------
    index_members (list): names/IDs of constituents of original index
    replication_members (list): names/IDs of assets in replication portfolio
    Returns
    -------
    float containing the ratio of intersection cardinality to union cardinality,
    returning 0 if two set has no common asset and 1 of two set has identical members
    """
    index_members = set(index_members)
    replication_members = set(replication_members)
    return len(index_members.intersection(replication_members))/len(index_members.union(replication_members))


def sorensen_coefficient(index_members, replication_members):
    """Calculate similarity between two sets of members in index constituents and replication portfolio members,
    by computing the ratio of twice of intersection cardinality to the sum of cardinalities of two set,
    namely in LaTeX: $\frac{2|X \cap Y|}{|X| + |Y|}$
    Parameters
    ----------
    index_members (list): names/IDs of constituents of original index
    replication_members (list): names/IDs of assets in replication portfolio
    Returns
    -------
    float containing the ratio described earlier,
    returning 0 if two set has no common asset and 1 of two set has identical members
    """
    index_members = set(index_members)
    replication_members = set(replication_members)
    return 2*len(index_members.intersection(replication_members))/(len(index_members) + len(replication_members))


def jaccard_index(index_weights, replication_weights):
    """Calculate similarity between two sets of members in index constituents and replication portfolio members,
    by computing the ratio of shared weight and total weight,
    namey in LaTeX: $\frac{\sum_i \min{(x_i, y_i)}}{\sum_i \max{(x_i, y_i)}}$
    Parameters
    ----------
    index_weights (list): weights of constituents of original index, indexed by union of all assets available,
    weights denoted as 0 if asset is not included in the original index
    replication_weights (list): weights of assets in replicating portfolio, indexed by union of all assets available,
    weights denoted as 0 if asset is not included in the replicating portfolio
    Returns
    -------
    float containing the ratio of intersection cardinality to union cardinality,
    returning 0 if two set has no common asset and 1 of two set has identical weights for each member
    """
    index_weights = np.asarray(index_weights)
    replication_weights = np.asarray(replication_weights)
    return np.sum(np.minimum(index_weights, replication_weights))/np.sum(np.maximum(index_weights, replication_weights))


def sorensen_index(index_weights, replication_weights):
    """Calculate similarity between two sets of members in index constituents and replication portfolio members,
    by computing the ratio of shared weight and total weight,
    namey in LaTeX: $\frac{2\sum_i \min{(x_i,y_i)}}{\sum_i (x_i+y_i)}$
    Parameters
    ----------
    index_weights (list): weights of constituents of original index, indexed by union of all assets available,
    weights denoted as 0 if asset is not included in the original index
    replication_weights (list): weights of assets in replicating portfolio, indexed by union of all assets available,
    weights denoted as 0 if asset is not included in the replicating portfolio
    Returns
    -------
    float containing the ratio of intersection cardinality to union cardinality,
    returning 0 if two set has no common asset and 1 of two set has identical weights for each member
    """
    index_weights = np.asarray(index_weights)
    replication_weights = np.asarray(replication_weights)
    return 2 * np.sum(np.minimum(index_weights, replication_weights))/np.sum(index_weights + replication_weights)


'''
---------
UNIT TEST
---------
'''


class TestTrackingError(unittest.TestCase):
    def test_tracking_error_one(self):
        """
        Test whether function tracking_error can return correct std of differences
        """
        data = [[1, 2, 3], [1, 2, 3]]
        result = tracking_error(data[0], data[1])
        self.assertEqual(result, 0)

    def test_tracking_error_two(self):
        """
        Test whether function tracking_error can return correct std of differences
        """
        data = [[1, 2, 3, 4], [5, 2, 3, 4]]
        result = tracking_error(data[0], data[1])
        self.assertEqual(result, 2.0)


class TestTrackingDifference(unittest.TestCase):
    def test_tracking_difference_one(self):
        """
        Test whether function tracking_difference can return correct mean abs of differences
        """
        data = [[1, 2, 3], [1, 2, 3]]
        result = tracking_difference(data[0], data[1])
        self.assertEqual(result, 0)

    def test_tracking_difference_two(self):
        """
        Test whether function tracking_difference can return correct mean abs of differences
        """
        data = [[1, 2, 3, 4], [2, 2, 4, 7]]
        result = tracking_difference(data[0], data[1])
        self.assertEqual(result, 1.25)


class TestJaccardSimilarity(unittest.TestCase):
    def test_jaccard_similarity_one(self):
        """
        Test whether function jaccard_difference can return correct ratio of set cardinality
        """
        data = [['AAPL', 'MSFT', 'BABA'], ['AMZN', 'GOOG', 'CSCO']]
        result = jaccard_similarity(data[0], data[1])
        self.assertEqual(result, 0)

    def test_jaccard_similarity_two(self):
        """
        Test whether function jaccard_difference can return correct ratio of set cardinality
        """
        data = [['AAPL', 'MSFT', 'BABA'], ['MSFT', 'AAPL', 'BABA']]
        result = jaccard_similarity(data[0], data[1])
        self.assertEqual(result, 1)

    def test_jaccard_similarity_three(self):
        """
        Test whether function jaccard_difference can return correct ratio of set cardinality
        """
        data = [['AAPL', 'MSFT', 'BABA', 'AMZN'], ['AMZN', 'GOOG', 'CSCO', 'AAPL', 'MSFT']]
        result = jaccard_similarity(data[0], data[1])
        self.assertEqual(result, 0.5)


class TestSorensenCoefficient(unittest.TestCase):
    def test_sorensen_coefficient_one(self):
        """
        Test whether function sorensen_coefficient can return correct ratio of set cardinality
        """
        data = [['AAPL', 'MSFT', 'BABA'], ['AMZN', 'GOOG', 'CSCO']]
        result = sorensen_coefficient(data[0], data[1])
        self.assertEqual(result, 0)

    def test_sorensen_coefficient_two(self):
        """
        Test whether function sorensen_coefficient can return correct ratio of set cardinality
        """
        data = [['AAPL', 'MSFT', 'BABA'], ['MSFT', 'AAPL', 'BABA']]
        result = sorensen_coefficient(data[0], data[1])
        self.assertEqual(result, 1)

    def test_sorensen_coefficient_three(self):
        """
        Test whether function sorensen_coefficient can return correct ratio of set cardinality
        """
        data = [['AAPL', 'MSFT', 'BABA', 'AMZN'], ['AMZN', 'GOOG', 'CSCO', 'AAPL', 'MSFT']]
        result = sorensen_coefficient(data[0], data[1])
        self.assertEqual(result, 2/3)


class TestJaccardIndex(unittest.TestCase):
    def test_jaccard_index_one(self):
        """
        Test whether function jaccard_index can return correct ratio of set cardinality
        """
        data = [[0.2, 0.3, 0.5, 0, 0, 0], [0, 0, 0, 0.2, 0.3, 0.5]]
        result = jaccard_index(data[0], data[1])
        self.assertEqual(result, 0)

    def test_jaccard_index_two(self):
        """
        Test whether function jaccard_index can return correct ratio of set cardinality
        """
        data = [[0.2, 0.3, 0.5, 0, 0, 0], [0.2, 0.3, 0.5, 0, 0, 0]]
        result = jaccard_index(data[0], data[1])
        self.assertEqual(result, 1)

    def test_jaccard_index_three(self):
        """
        Test whether function jaccard_index can return correct ratio of set cardinality
        """
        data = [[0.2, 0.3, 0.5, 0, 0, 0], [0, 0.2, 0.3, 0.5, 0, 0]]
        result = jaccard_index(data[0], data[1])
        self.assertEqual(result, 1/3)


class TestSorensenIndex(unittest.TestCase):
    def test_sorensen_index_one(self):
        """
        Test whether function sorensen_index can return correct ratio of set cardinality
        """
        data = [[0.2, 0.3, 0.5, 0, 0, 0], [0, 0, 0, 0.2, 0.3, 0.5]]
        result = sorensen_index(data[0], data[1])
        self.assertEqual(result, 0)

    def test_sorensen_index_two(self):
        """
        Test whether function sorensen_index can return correct ratio of set cardinality
        """
        data = [[0.2, 0.3, 0.5, 0, 0, 0], [0.2, 0.3, 0.5, 0, 0, 0]]
        result = sorensen_index(data[0], data[1])
        self.assertEqual(result, 1)

    def test_sorensen_index_three(self):
        """
        Test whether function sorensen_index can return correct ratio of set cardinality
        """
        data = [[0.2, 0.3, 0.5, 0, 0, 0], [0, 0.2, 0.3, 0.5, 0, 0]]
        result = sorensen_index(data[0], data[1])
        self.assertEqual(result, 1/2)


if __name__ == '__main__':
    unittest.main()